package com.company;

public class Main {


    public static void main(String[] args) {


        //Правило Вансдорфа

        int[][] board = new int[5][5];

        for (int yy = 0; yy < 5; yy++) {
            for (int xx = 0; xx < 5; xx++) {
                board[yy][xx] = 0;
            }
        }


        //конь ходит
        //board[y - 2][x - 1] = 1;
        //board[y - 1][x - 2] = 1;
        //board[y + 1][x - 2] = 1;
        //board[y + 2][x - 1] = 1;
        //board[y + 2][x + 1] = 1;
        //board[y + 1][x + 2] = 1;
        //board[y - 1][x + 2] = 1;
        //board[y - 2][x + 1] = 1;

        CoordinatesHorse hor = new CoordinatesHorse();
        hor.y = 2;
        hor.x = 2;
        board[hor.y][hor.x] = 1;
        int stepY;
        int stepX;


        while (true) {
            int step = (int) (Math.random() * 8);

            switch (step) {
                case 0:
                    stepY = hor.y - 2;
                    stepX = hor.x - 1;
                    step(hor, board, stepY, stepX);

                    break;

                case 1:
                    stepY = hor.y - 1;
                    stepX = hor.x - 2;
                    step(hor, board, stepY, stepX);

                    break;

                case 2:
                    stepY = hor.y + 1;
                    stepX = hor.x - 2;
                    step(hor, board, stepY, stepX);

                    break;

                case 3:
                    stepY = hor.y + 2;
                    stepX = hor.x - 1;
                    step(hor, board, stepY, stepX);

                    break;

                case 4:
                    stepY = hor.y + 2;
                    stepX = hor.x + 1;
                    step(hor, board, stepY, stepX);

                    break;

                case 5:
                    stepY = hor.y + 1;
                    stepX = hor.x + 2;
                    step(hor, board, stepY, stepX);

                    break;

                case 6:
                    stepY = hor.y - 1;
                    stepX = hor.x + 2;
                    step(hor, board, stepY, stepX);

                    break;

                case 7:
                    stepY = hor.y - 2;
                    stepX = hor.x + 1;
                    step(hor, board, stepY, stepX);

                    break;
            }


            if (hor.deadlock == 7) {
                hor.y = 2;
                hor.x = 2;

                for (int yy = 0; yy < 5; yy++) {
                    for (int xx = 0; xx < 5; xx++) {
                        board[yy][xx] = 0;
                    }
                }
            }


            int teamp = 1;
            for (int yy = 0; yy < 5; yy++) {
                for (int xx = 0; xx < 5; xx++) {
                   if(board[yy][xx] == 1)
                       teamp++;

                }
            }


            if(board.length * board.length == teamp)
                break;
        }




        for (int yy = 0; yy < 5; yy++) {
            for (int xx = 0; xx < 5; xx++) {
                System.out.print(board[yy][xx] + " ");
            }
            System.out.println();
        }


    }


    private static void step(CoordinatesHorse hor,
                             int[][] board,
                             int stepY,
                             int stepX) {

        if((stepY > -1 && stepX > -1) && (stepY < 5 && stepX < 5)){
            if (board[stepY][stepX] != 1) {
                hor.deadlock = 0;
                board[stepY][stepX] = 1;
                hor.y = stepY;
                hor.x = stepX;


            } else {
                hor.deadlock++;
            }
        }

    }


    public static class CoordinatesHorse {
        public int y;
        public int x;
        public int deadlock;
    }


}
